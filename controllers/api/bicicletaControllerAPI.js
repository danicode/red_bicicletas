var Bicicleta = require('../../models/bicicleta')

exports.bicicleta_list = function(req, res) {
  Bicicleta.allBicis().exec((err, bicis) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      })
    }
    res.status(200).json({
      bicicletas: bicis
    })
  })
}

exports.bicicleta_create = function(req, res) {
  var bici = new Bicicleta({
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo
  })
  bici.ubicacion = [req.body.lat, req.body.lng]

  Bicicleta.add(bici, function(err, newBici) {
    if (err) console.log(err)
    res.status(200).json({
      bicicleta: newBici
    })
  })
}

exports.bicicleta_delete = function(req, res) {
  Bicicleta.removeByCode(req.body.code)
  res.status(204).send()
}

exports.bicicleta_update = function(req, res) {

  let aBici = { 
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [
      req.body.lat,
      req.body.lng
    ]
  }

  Bicicleta.update(aBici)
    .then((update) => {
      res.status(200).json({
        bicicleta: update
      })
    }).catch(e=>{
      res.status(401).json({
        error: e
      })
    })
}
