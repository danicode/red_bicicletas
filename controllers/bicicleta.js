var Bicicleta = require('../models/bicicleta')

exports.bicicleta_list = (req, res, next) => {
  Bicicleta.find({})
    .then((bicis) => {
      res.render("bicicletas/index", { bicis })
    })
},

exports.bicicleta_create_get = function(req, res) {
  res.render('bicicletas/create')
}

exports.bicicleta_create_post = function(req, res) {
  let code = req.body.code
  let color = req.body.color
  let modelo = req.body.modelo
  let ubicacion = []
  ubicacion[0] = req.body.lat
  ubicacion[1] = req.body.lng
  var bici = new Bicicleta({ code, color, modelo, ubicacion })
  Bicicleta.add(bici)
  res.redirect('/bicicletas')
}

exports.bicicleta_update_get = (req, res, next) => {
  Bicicleta.findById(req.params.id)
    .then((bici) => {
      res.render("bicicletas/update", { errors: {}, bici })
    })
},

exports.bicicleta_update_post = function (req, res) {
  let body = _.pick(req.body, ['color', 'modelo', 'code'])
  body.ubicacion = []
  if (req.body.lat) {
    body.ubicacion[0] = req.body.lat
  }
  if (req.body.lng) {
    body.ubicacion[1] = req.body.lng
  }

  Bicicleta.findByIdAndUpdate(
    req.params.id,
    body, {
      new: true
    },
    (err, biciBD) => {
      return res.redirect('/bicicletas')
    })
}

exports.bicicleta_delete_post =  (req, res, next) => {
  Bicicleta.findByIdAndDelete(req.body.id)
    .then(() => {
      res.redirect("/bicicletas")
    })
    .catch((err) => {
      next(err)
    })
}

exports.bicicleta_show_get = (req, res, next) => {
  Bicicleta.findById(req.params.id)
    .then((bici) => {
      res.render("bicicletas/show", { errors: {}, bici })
    })
}