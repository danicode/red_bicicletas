var mymap = L.map('main_map').setView([-27.4553681,-58.977969], 13)

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(mymap)

// L.marker([-27.451408, -58.985405]).addTo(mymap)
// L.marker([-27.451855, -58.987572]).addTo(mymap)
// L.marker([-27.450383, -58.987557]).addTo(mymap)

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result) {
    console.log(result)
    result.bicicletas.forEach(function(bici) {
      L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap)
    })
  }
})