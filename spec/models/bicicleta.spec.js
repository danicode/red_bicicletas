var mongoose = require('mongoose')
const Bicicleta = require('../../models/bicicleta')

describe('Testing Bicicletas', function() {
  

  // beforeEach(function (done) {
  //   var mongoDB = 'mongodb://localhost/testdb'
  //   mongoose.connect(mongoDB, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })

  //   const db = mongoose.connection
  //   db.on('error', console.error.bind(console, 'connection error'))
  //   db.once('open', function() {
  //     console.log('We are connected to test database!')
  //     done()
  //   })
  // })

  beforeAll(function(done) {

    mongoose.connection.close().then(() => {

      var mongoDB = 'mongodb://localhost/testdb'
      mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true })
      mongoose.set('useCreateIndex', true)

      var db = mongoose.connection
      db.on('error', console.error.bind(console, 'MongoDB connection error: '))
      db.once('open', function () {
        console.log('We are connected to test database!')
        done()
      })
    })
  })

  afterEach(function(done) {
    Bicicleta.deleteMany({}, function(err, success) {
      if (err) console.err(err)
      done()
    })
  })

  describe('Bicicleta.createInstance', () => {

    it('crea una instancia de Bicicleta', (done) => {
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [-27.451698, -58.990569])

      expect(bici.code).toBe(1)
      expect(bici.color).toBe("verde")
      expect(bici.modelo).toBe("urbana")
      expect(bici.ubicacion[0]).toEqual(-27.451698)
      expect(bici.ubicacion[1]).toEqual(-58.990569)
      done()
    })
  })

  describe('Bicicleta.allBicis', () => {
    it('comienza vacia', (done) => {
      Bicicleta.allBicis(function(err, bicis) {
        expect(bicis.length).toBe(0)
        done()
      })
    })
  })

  describe('Bicicleta.add', () => {
    it('agregar solo una bici', (done) => {
      var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"})
      Bicicleta.add(aBici, function(err, newBici) {
        if (err) console.err(err)
        Bicicleta.allBicis(function(err, bicis) {
          expect(bicis.length).toEqual(1)
          expect(bicis[0].code).toEqual(aBici.code)
          done()
        })
      })
    })
  })

  describe('Bicicletas.findByCode', () => {
    it('Debe devolver la bici con code 1', (done) => {
      Bicicleta.find({}, (err, bicis) => {
        expect(bicis.length).toBe(0)

        var aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'urbana' })
        Bicicleta.add(aBici, function (err, newBici) {
          if (err) console.err(err)

          var aBici2 = new Bicicleta({ code: 2, color: 'roja', modelo: 'urbana' })
          Bicicleta.add(aBici2, function (err, newBici) {
            if (err) console.err(err)
            Bicicleta.findByCode(1, function (errr, targetBici) {
              expect(targetBici.code).toEqual(aBici.code)
              expect(targetBici.color).toEqual(aBici.color)
              expect(targetBici.modelo).toEqual(aBici.modelo)
              done()
            })
          })
        })
      })
    })
  })

  describe('Bicicletas.removeByCode', () => {
    it('Debe eliminar la bici con code 1', (done) => {
      Bicicleta.find({}, (err, bicis) => {
        expect(bicis.length).toBe(0)

        let aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'urbana' })
        Bicicleta.add(aBici, function (err, newBici) {
          if (err) console.err(err)

          let aBici2 = new Bicicleta({ code: 2, color: 'roja', modelo: 'urbana' })
          Bicicleta.add(aBici2, function (err, newBici) {
            if (err) console.err(err)
            Bicicleta.removeByCode(1).then((targetBici) => {
                Bicicleta.find({}).then((bicis) => {
                  expect(bicis.length).toBe(1)
                  done()
                })
            }).catch((err) => {
              console.err(err)
            })
          })
        })
      })
    })
  })
})
