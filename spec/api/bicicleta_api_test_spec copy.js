var Bicicleta = require('../../models/bicicleta')
var request = require('request')
var server = require('../../bin/www')
const { ExpectationFailed } = require('http-errors')

describe('Bicicleta API', () => {

  beforeEach(() => {
    Bicicleta.allBicis = []
  })

  describe('GET BICICLETAS /', () => {
    it('Status 200', () => {
      expect(Bicicleta.allBicis.length).toBe(0)

      var a = new Bicicleta(1, 'negro', 'urbana', [-27.451698, -58.990569])
      Bicicleta.add(a)

      request.get('http://localhost:5000/api/bicicletas', function(error, response, body) {
        expect(response.statusCode).toBe(200)
      })
    })
  })

  describe('POST BICICLETAS /create', () => {
    it('Status 200', (done) => {
      expect(Bicicleta.allBicis.length).toBe(0)
      var headers = {'Content-Type' : 'application/json'}
      var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -27, "lng": -58}'
      request.post({
          headers: headers,
          url: 'http://localhost:5000/api/bicicletas/create',
          body: aBici
        }, function(error, response, body) {
            expect(response.statusCode).toBe(200)
            expect(Bicicleta.allBicis.length).toBe(1)
            expect(Bicicleta.findById(10).color).toBe("rojo")
            done()
      })
    })
  })

  describe('PUT BICICLETAS /update', () => {
    it('Status 200', (done) => {
      expect(Bicicleta.allBicis.length).toBe(0)

      var a = new Bicicleta(1, 'negro', 'urbana', [-27.451698, -58.990569])
      Bicicleta.add(a)

      var headers = {'Content-Type' : 'application/json'}
      var aBici = '{"id": 1, "color": "rojo", "modelo": "urbana", "lat": -27, "lng": -58}'
      request.put({
          headers: headers,
          url: 'http://localhost:5000/api/bicicletas/update',
          body: aBici
        }, function(error, response, body) {
            expect(response.statusCode).toBe(200)
            expect(Bicicleta.findById(1).color).toBe("rojo")
            expect(Bicicleta.findById(1).modelo).toBe("urbana")
            done()
      })
    })
  })

  describe('DELETE BICICLETAS /delete', () => {
    it('Status 204', (done) => {
      expect(Bicicleta.allBicis.length).toBe(0)

      var a = new Bicicleta(1, 'negro', 'urbana', [-27.451698, -58.990569])
      Bicicleta.add(a)

      var headers = {'Content-Type' : 'application/json'}
      var body = '{"id": 1}'
      request.delete({
          headers: headers,
          url: 'http://localhost:5000/api/bicicletas/delete',
          body: body
        }, function(error, response, body) {
            expect(response.statusCode).toBe(204)
            expect(Bicicleta.allBicis.length).toBe(0)
            done()
      })
    })
  })
})
