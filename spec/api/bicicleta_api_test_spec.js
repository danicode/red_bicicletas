var mongoose = require('mongoose')
var Bicicleta = require("../../models/bicicleta")
var server = require('../../bin/www')
var request = require('request')

var base_url = "http://localhost:5000/api/bicicletas"

describe('Bicicleta API', () => {

  // beforeEach((done) => {
  //   var mongoDB = 'mongodb://localhost/testdb'
  //   mongoose.connect(mongoDB, {
  //     useNewUrlParser: true,
  //     useUnifiedTopology: true
  //   })
  //   mongoose.set('useCreateIndex', true)

  //   const db = mongoose.connection;
  //   db.on('error', console.error.bind(console, 'Connection error'))
  //   db.once('open', () => {
  //     console.log("conectado a la BD")
  //     done()
  //   })
  // })

  beforeAll(function(done) {

    mongoose.connection.close().then(() => {

      var mongoDB = 'mongodb://localhost/testdb'
      mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true })
      mongoose.set('useCreateIndex', true)

      var db = mongoose.connection
      db.on('error', console.error.bind(console, 'MongoDB connection error: '))
      db.once('open', function () {
        console.log('We are connected to test database!')
        done()
      })
    })
  })

  afterEach((done) => {
    Bicicleta.deleteMany({}, (err, success) => {
      if (err) console.err(err)
      done()
    })
  })

  describe('GET BICICLETAS / ', () => {
    it('STATUS 200', (done) => {
      request.get(base_url, function (error, response, body) {
        var result = JSON.parse(body)
        expect(response.statusCode).toBe(200)
        expect(result.bicicletas.length).toBe(0)
        done()
      })
    })
  })

  describe('POST BICICLETAS /create', () => {
    it('STATUS 200', (done) => {
      var headers = {'Content-Type' : 'application/json'}
      var aBici = '{"code": 10, "color": "rojo", "modelo": "urbana", "lat": -27.451698, "lng": -58.990569}'
      request.post({
          headers: headers,
          url: base_url + '/create',
          body: aBici
        }, function(error, response, body) {
          expect(response.statusCode).toBe(200)
          let bici = JSON.parse(body).bicicleta
          expect(bici.code).toBe(10)
          expect(bici.color).toBe("rojo")
          expect(bici.modelo).toBe("urbana")
          expect(bici.ubicacion[0]).toBe(-27.451698)
          expect(bici.ubicacion[1]).toBe(-58.990569)
          done()
        })
    })
  })

  describe('DELETE BICICLETAS /delete', () => {
    it('STATUS 204', (done) => {

      //Se crea el registro a actualizar
      var a = Bicicleta.createInstance(1, "negro", "urbana", [-27.451698, -58.990569])
      Bicicleta.add(a, function(err, newBici) {
        var headers = {'Content-Type' : 'application/json'}
        var body = '{"code": 1}'
        request.delete({
          headers: headers,
          url: base_url + '/delete',
          body: body
        }, function(error, response, body) {
            expect(response.statusCode).toBe(204)
            expect(Bicicleta.allBicis.length).toBe(1)
            done()
        })
      })
    })
  })

  describe('PUT BICICLETAS /update', () => {
    it('STATUS 200', (done) => {
      var headers = {
        'Content-Type': 'application/json'
      }
      var a = Bicicleta.createInstance(2, "negro", "urbana", [-27.451698, -58.990569])
      Bicicleta.add(a, function(err, newBici) {
        var headers = {'Content-Type' : 'application/json'}
        var aBici = '{"code": 2, "color": "rojo", "modelo": "urbana", "lat": -27, "lng": -58}'
        request.put({
          headers: headers,
          url: base_url + '/update',
          body: aBici
        }, function(error, response, body) {
            let bici = JSON.parse(body).bicicleta
            expect(response.statusCode).toBe(200)
            expect(bici.color).toBe("rojo")
            expect(bici.modelo).toBe("urbana")
            done()
        })
      })
    })
  })

})
