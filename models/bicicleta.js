var mongoose = require('mongoose')
var Schema = mongoose.Schema

var bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number], index: { type: '2dsphere', sparse: true }
  }
})

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
  return new this({
    code,
    color,
    modelo,
    ubicacion: ubicacion
  })
}

bicicletaSchema.methods.toString = function() {
  return 'code: ' + this.code + ' | color: ' + this.color
}

bicicletaSchema.statics.allBicis = function(cb) {
  return this.find({}, cb)
}

bicicletaSchema.statics.add = function(aBici, cb) {
  return this.create(aBici, cb)
}

// bicicletaSchema.statics.findByCode = function(aCode, cb) {
//   return this.findOne({code: aCode}, cb)
// }

// bicicletaSchema.statics.removeByCode = function(aCode, cb) {
//   return this.deleteOne({code: aCode}, cb)
// }

bicicletaSchema.statics.update = function async(aBici, cb) {
  return new Promise(async (resolve, reject) => {
    try {
      let updateBici = await this.findById(aBici.id)
      updateBici.color = aBici.color
      updateBici.modelo = aBici.modelo
      updateBici.ubicacion = aBici.ubicacion
      this.updateOne(updateBici, function() {
        resolve(updateBici)
        return true
      })
    } catch (e) {
      reject(e)
      return false
    }
  })
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema)
