# Red de bicicletas
Proyecto realizado para el curso: Desarrollo del lado servidor: NodeJS, Express y MongoDB en la plataforma Coursera.
El proyecto consiste en un CRUD para gestionar bicicletas, además cuenta con un mapa donde visualizarás las bicicletas y también una pequeña API.
## Iniciando el proyecto
- Descargar depedencias con el siguiente comando
```npm install```
- Arrancar el proyecto con el siguiente comando
```npm start```
- En entorno local lo puedes acceder de la siguiente manera
```http://127.0.0.1:3000```