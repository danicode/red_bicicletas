require('newrelic')
require('dotenv').config()
const assert = require('assert')
var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
const passport = require('./config/passport')
const session = require('express-session')
const jwt = require('jsonwebtoken')

var welcomeExpressRouter = require('./routes/welcomeExpress')
var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var usuariosRouter = require('./routes/usuarios')
var tokenRouter = require('./routes/token')

var bicicletasRouter = require('./routes/bicicletas')
var bicicletasAPIRouter = require('./routes/api/bicicletas')
var usuarioAPIRouter = require('./routes/api/usuarios')
const authAPIRouter = require('./routes/api/auth')

const mongoDBStore = require('connect-mongodb-session')(session)

const Token = require('./models/token')
const Usuario = require('./models/usuario')


var app = express()

app.set('secretKey', 'jwt_pwd_!12233445')

//esto se reemplazo porque lo que esta debajo
//const store = new session.MemoryStore
let store
if (process.env.NODE_ENV === "development") {
  store = new session.MemoryStore
} else {
  store = new mongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  })
  store.on('error', (error) => {
    assert.ifError(error)
    assert.ok(false)
  })
}

app.use(session({
  cookie: { maxAgre: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: true,
  secret: 'red_bici!"·!$"·%·$·$&%%·$__________'
}))

var mongoose = require('mongoose')

var mongoDB = process.env.MONGO_URI
mongoose.connect(mongoDB, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
mongoose.Promise = global.Promise
var db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connecion error: '))

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(passport.initialize())
app.use(passport.session())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', welcomeExpressRouter)
app.use('/index', indexRouter)
app.use('/users', usersRouter)
app.use('/usuarios', usuariosRouter)
app.use('/token', tokenRouter)

app.use('/bicicletas', loggedIn, bicicletasRouter)
app.use('/api/usuarios', usuarioAPIRouter)
app.use('/api/auth', authAPIRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter)

app.get('/auth/google',
  passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/plus.profile.emails.read',
    'profile',
    'email'
  ]}
))

app.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/',
  failureRedirect: '/error'
}))

app.get('/login', (req, res) => {
  res.render('session/login');
});

app.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, usuario, info) => {
    if (err) {
      return next(err)
    }
    if (!usuario) return res.render('session/login', { info })
    req.logIn(usuario, function (err) {
      if (err) {
        return next(err)
      }
      return res.redirect('/index')
    })
  })(req, res, next)
})

app.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/login')
})

app.get("/forgotPassword", (req, res) => {
  res.render("session/forgotPassword")
})

app.post("/forgotPassword", (req, res, next) => {
  const { email } = req.body
  Usuario.findOne({ email })
    .then((usuario) => {
      if (!usuario)
        return res.render("session/forgotPassword", {
          info: { message: "No existe el email para un usuario existente" },
        })
      usuario.resetPassword((err) => {
        if (err) return next(err);
        console.log("session/forgotPasswordMessage");
      })
      res.render("session/forgotPasswordMessage");
    })
})

app.get("/resetPassword/:token", (req, res, next) => {
  const { token } = req.params
  Token.findOne({ token })
    .then((token) => {
      if (!token)
        return res
          .status(400)
          .send({ type: "not-verified", msg: "No existe token un usuario asociado al token. Verifique que su token no haya expirado" })
      Usuario.findById(token._userId)
        .then((usuario) => {
          if (!usuario)
            return res
              .status(400)
              .send({ type: "not-verified", msg: "No existe token un usuario asociado al token." })
          res.render("session/resetPassword", { errors: {}, usuario })
        })
    })
})

app.post("/resetPassword", (req, res) => {
  const { email, password, confirm_password } = req.body
  if (password !== confirm_password) {
    res.render("session/resetPassword", {
      errors: { confirm_password: "No coincide el password" },
      usuario: new Usuario({ email }),
    })
    return
  }
  Usuario.findOne({ email })
    .then((usuario) => {
      usuario.password = password
      usuario.save()
        .then(() => {
          res.redirect("/login")
        })
    })
    .catch((err) => {
      res.render("session/resetPassword", {
        errors: err.errors,
        usuario: new Usuario({ email }),
      })
    })
})

app.get('/privacy_policy', (req, res) => {
  res.sendFile('public/privacy_policy.html', { root: '.' })
})

app.get('/sitemap', (req, res) => {
  res.sendFile('public/sitemap.xml', { root: '.' })
})

// app.get('/google17eda75ecf2a1677', (req, res) => {
//   res.sendFile('public/google17eda75ecf2a1677.html', { root: '.' })
// })

app.get('/google17eda75ecf2a1677', (req, res) => {
  res.sendFile('public/google17eda75ecf2a1677.html')
})

// app.get('/auth/google/callback',
//   passport.authenticate('google', {
//     failureRedirect: '/login',
//     successRedirect: "/bicicletas",
//   }))
  // ,
  // function(req, res) {
  //   res.redirect('/bicicletas')
  // })

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

function loggedIn(req, res, next) {
  if (req.user) {
    next()
  } else {
    console.log('Usuario sin loguearse');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
    if (err) {
      res.json({
        status: "error",
        message: err.message,
        data: null
      });
    } else {
      req.body.userId = decoded.id
      console.log('jwt verifyt: ', decoded);
      next();
    }
  })
}

module.exports = app
